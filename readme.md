# Cli application for parse day.lt day's names

### How to run

`$ php cli.php --start <start date> --end <end date> --strategy <strategy> --output <output>`

**<start date>** and **<end date>** should be in format Y.m.d

**<strategy>** string, one of "dom" or "regex" (without quotes)

* **dom** strategy use DOMDocument and DOMXPath

* **regex** strategy use file_get_contents and regular expression

**<output>** string, one of "json" or "xml" (without quotes)

### Example

`php cli.php --start 2018.06.01 --end 2018.06.10 --strategy dom --output json`


### Demo

![demo](https://i.imgur.com/SyoGwsa.gif)

https://i.imgur.com/SyoGwsa.gif


### Bonus :)
Bash "one-liner" for parse names for date:

`curl --silent https://day.lt/diena/2018.06.01 | iconv -f cp1257 -t utf8 | pcregrep -o1 '<a href="vardai/[^>]+>(.*?)</a>'`

![demo](https://i.imgur.com/3sefqZo.gif)

https://i.imgur.com/3sefqZo.gif
