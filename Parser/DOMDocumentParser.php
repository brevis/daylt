<?php

namespace TestTask\Parser;


class DOMDocumentParser extends NamesParser
{
    /**
     * @inheritdoc
     */
    public function getNamesForDate($dateString)
    {
        $names = array();

        $doc = new \DOMDocument();
        libxml_use_internal_errors(true);
        try {
            $doc->loadHTMLFile(sprintf(self::URL_FORMAT, $dateString));
        } catch (\Exception $e) {
            return $names;
        }

        $xpath = new \DOMXPath($doc);
        $namesEls = $xpath->query('//*[@id="mc"]//tr[2]/td[3]/div[3]/p/a');
        foreach ($namesEls as $el) {
            $names[] = $el->textContent;
        }

        return $names;
    }
}