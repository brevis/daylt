<?php

namespace TestTask\Parser;


class RegexParser extends NamesParser
{
    const TIMEOUT = 3;

    public function getNamesForDate($dateString)
    {
        $html = $this->downloadPage(sprintf(self::URL_FORMAT, $dateString));
        $names = array();
        if (preg_match_all('/<a href="vardai\/[^>]+>(.*)<\/a>/U', $html, $match)) {
            $names = $match[1];
        }
        return $names;
    }

    protected function downloadPage($url)
    {
        $ctx = stream_context_create(array(
            'http' => array(
                'timeout' => self::TIMEOUT,
            )
        ));

        return self::convertEncoding(file_get_contents($url, false, $ctx));
    }
}