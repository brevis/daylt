<?php

namespace TestTask\Parser;

abstract class NamesParser
{

    const URL_FORMAT = 'https://day.lt/diena/%s';

    const INPUT_ENCODING = 'cp1257';

    const OUTPUT_ENCODING = 'utf8';

    /**
     * Return names for date
     *
     * @param string $dateString
     * @return array
     */
    abstract function getNamesForDate($dateString);

    /**
     * Return concrete NamesParser based on strategy
     *
     * @param $strategy
     * @return NamesParser
     */
    public static function getParser($strategy)
    {
        switch ($strategy) {
            case 'dom':
                return new DOMDocumentParser();

            case 'regex':
                return new RegexParser();

            default:
                throw new \InvalidArgumentException("Wrong strategy");
        }
    }

    public static function convertEncoding($string, $inputEncoding = self::INPUT_ENCODING, $outputEncoding = self::OUTPUT_ENCODING)
    {
        return iconv($inputEncoding, $outputEncoding, $string);
    }
}