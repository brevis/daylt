<?php

namespace TestTask\Transformer;

abstract class DataTransformer
{
    /**
     * Transform input data
     *
     * @param array $input
     * @return string
     */
    abstract public function transform($input);

    /**
     * Return DataTransformer based on strategy
     *
     * @param $strategy
     * @return DataTransformer
     */
    public static function getTransformer($strategy)
    {
        switch ($strategy) {
            case 'json':
                return new JsonTransformer();

            case 'xml':
                return new XmlTransformer();

            default:
                throw new \InvalidArgumentException("Wrong strategy");
        }
    }
}