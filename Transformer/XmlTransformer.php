<?php

namespace TestTask\Transformer;

class XmlTransformer extends DataTransformer
{
    /**
     * Transform input data to xml string
     *
     * @param $input
     * @return string
     */
    public function transform($input)
    {
        $dom = new \DOMDocument('1.0', 'utf-8');
        $root = $dom->createElement('root');
        foreach ($input as $date=>$names) {
            $day = $dom->createElement('day');
            $day->appendChild($dom->createElement('date', $date));
            $namesEl = $dom->createElement('names');
            foreach ($names as $name) {
                $namesEl->appendChild($dom->createElement('name', $name));
            }
            $day->appendChild($namesEl);
            $root->appendChild($day);
        }
        $dom->appendChild($root);
        return $dom->saveXML(); 
    }
}