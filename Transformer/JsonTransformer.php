<?php

namespace TestTask\Transformer;

class JsonTransformer extends DataTransformer
{
    /**
     * Transform input data to json string
     *
     * @param $input
     * @return string
     */
    public function transform($input)
    {
        return json_encode($input, JSON_UNESCAPED_UNICODE);
    }
}