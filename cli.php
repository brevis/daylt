<?php

namespace TestTask;

use TestTask\Parser\NamesParser;
use TestTask\Transformer\DataTransformer;

class TestTaskCli {

    const DATE_FORMAT = 'Y.m.d';

    /**
     * App entry point
     */
    public static function run()
    {
        try {
            $arguments = self::getPassedArguments(true);
        } catch (\Exception $e) {
            printf("Error: %s\n", $e->getMessage());
            exit(1);
        }

        /** @var NamesParser $parser */
        $parser = NamesParser::getParser($arguments['strategy']);
        $currentDate = \DateTime::createFromFormat(self::DATE_FORMAT, $arguments['start']);
        $endDate = \DateTime::createFromFormat(self::DATE_FORMAT, $arguments['end']);
        $names = array();
        while ($currentDate <= $endDate) {
            $currentDateString = $currentDate->format(self::DATE_FORMAT);
            $names[$currentDateString] = $parser->getNamesForDate($currentDateString);
            $currentDate->modify("+1 day");
        }

        /** @var DataTransformer $transformer */
        $transformer = DataTransformer::getTransformer($arguments['output']);
        echo $transformer->transform($names) . PHP_EOL;
        exit(0);
    }

    /**
     * Get passed arguments
     *
     * @param bool $validate
     * @return array
     */
    protected static function getPassedArguments($validate = false)
    {
        $args = array();
        if (isset($_SERVER['argv']) && is_array($_SERVER['argv'])) {
            array_shift($_SERVER['argv']);
            $argName = '';
            foreach ($_SERVER['argv'] as $k => $v) {
                if ($k % 2 == 0) {
                    $argName = ltrim($v, '-');
                } else {
                    $args[$argName] = $v;
                    $argName = '';
                }
            }
        }

        if ($validate) {
            self::validatePassedArguments($args);
        }

        return $args;
    }

    /**
     * Validate passed arguments
     *
     * @param $args
     * @throws \InvalidArgumentException
     * @throws \RangeException
     */
    protected static function validatePassedArguments($args)
    {
        if (count($args) < 4) {
            echo 'Usage: php cli.php --start d.m.Y --end d.m.Y --strategy dom|regex --output json|xml' . PHP_EOL;
            exit(0);
        }

        if (!isset($args['start'])) {
            throw new \InvalidArgumentException("Start date should be passed");
        }

        if (!self::validateDate($args['start'])) {
            throw new \InvalidArgumentException("Invalid start date");
        }

        if (!isset($args['end'])) {
            throw new \InvalidArgumentException("Start date should be passed");
        }

        if (!self::validateDate($args['end'])) {
            throw new \InvalidArgumentException("Invalid end date");
        }

        $startDate = \DateTime::createFromFormat(self::DATE_FORMAT, $args['start']);
        $endDate = \DateTime::createFromFormat(self::DATE_FORMAT, $args['end']);
        if ($startDate > $endDate) {
            throw new \RangeException("Start date should be <= end date");
        }

        if (!isset($args['strategy'])) {
            throw new \InvalidArgumentException("Strategy should be passed");
        }

        if (!in_array($args['strategy'], array('dom', 'regex'), true)) {
            throw new \InvalidArgumentException("Strategy should be \"dom\" or \"regex\"");
        }

        if (!isset($args['output'])) {
            throw new \InvalidArgumentException("Output should be passed");
        }

        if (!in_array($args['output'], array('json', 'xml'), true)) {
            throw new \InvalidArgumentException("Output should be \"json\" or \"xml\"");
        }

    }

    /**
     * Validate date
     *
     * @param $date
     * @param string $format
     * @return bool
     */
    protected static function validateDate($date, $format = self::DATE_FORMAT)
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

}

spl_autoload_register(function ($class) {
    $class = preg_replace('/^TestTask\\\/', '', $class);
    include __DIR__ . '/' . str_replace("\\", "/", $class) . '.php';
});

TestTaskCli::run();
